require_relative '../gilded_rose'

RSpec.describe 'GildedRose' do
  it 'decreases 1 point quality each day' do
    item = Item.new('normal_item', 1, 1)
    gilded_rose = GildedRose.new([item])

    items = gilded_rose.update_quality

    expect(items.first.quality).to eq(0)
  end

  context 'conjured items' do
    it 'decreases double the quality each day' do
      name = 'conjured_item'
      sell_in = 1
      quality = 2
      conjured_item = ConjuredItem.new(name, sell_in, quality)
      gilded_rose = GildedRose.new([conjured_item])

      items = gilded_rose.update_quality

      conjured_item = items.first
      expect(conjured_item.quality).to eq(0)
    end

    it 'does not decrease quality under 0' do
      name = 'conjured_item'
      sell_in = 1
      quality = 1
      conjured_item = ConjuredItem.new(name, sell_in, quality)
      gilded_rose = GildedRose.new([conjured_item])

      items = gilded_rose.update_quality

      conjured_item = items.first
      expect(conjured_item.quality).to eq(0)
    end
  end
end
